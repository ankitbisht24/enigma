package utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailReports {

	public static Properties mailElements;

	public static void sendMailOfReport(String sPathforPropertyFile,
			String sPath, String sHTML) throws IOException {
		FileInputStream fs = new FileInputStream(sPathforPropertyFile);
		mailElements = new Properties(System.getProperties());
		mailElements.load(fs);

		Session session = Session.getDefaultInstance(mailElements,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(mailElements
								.getProperty("username"), mailElements
								.getProperty("password"));
					}
				});

		try {

			Message message = new MimeMessage(session);
			Multipart multipart = new MimeMultipart();
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			message.setFrom(new InternetAddress(mailElements
					.getProperty("username")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress
					.parse(mailElements.getProperty("toAddress")));
//			message.setRecipients(Message.RecipientType.CC, InternetAddress
//					.parse(mailElements.getProperty("ccAddress")));
			message.setRecipients(Message.RecipientType.BCC, InternetAddress
					.parse(mailElements.getProperty("bccAddress")));
			message.setSubject("Daily QA Status Report");

			String msg = mailElements.getProperty("mail.body") + sHTML
					+ mailElements.getProperty("mail.signature");

			messageBodyPart.setContent(msg, "text/html");

			multipart.addBodyPart(messageBodyPart);

			// adds attachments
			// if (mailElements.getProperty("mail.attachmentFilePath").length()
			// > 0) {
			// // for (String filePath : attachFiles) {
			// MimeBodyPart attachPart = new MimeBodyPart();
			// try {
			// attachPart.attachFile(mailElements
			// .getProperty("mail.attachmentFilePath"));
			//
			// } catch (IOException ex) {
			// Log.fatal("Sending Failed -- Unable to Add attachment to mail.");
			// Log.warn(ex.getMessage());
			// }
			//
			// multipart.addBodyPart(attachPart);
			// end of for loop
			// }

			// sets the multi-part as e-mail's content
			message.setContent(multipart);

			Transport.send(message);

		} catch (MessagingException e) {
			e.toString();
		}

	}// End of setPropertyFile
}
